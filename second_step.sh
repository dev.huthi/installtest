#!/bin/bash
set -euo pipefail

myusername="werner"
myhostname="alnx"
mypw="a"
mypwconfirm="b"

while [ $mypw != $mypwconfirm ]
do
	read -sp "Password: " mypw
	echo
	read -sp "Confirm Password: " mypwconfirm
	echo
done


sed /etc/locale.gen -e 's/#en_US.UTF-8/en_US.UTF-8/' -e 's/#de_DE.UTF-8/de_DE.UTF-8/' -i
locale-gen
echo "LANG=en_US.UTF-8
LC_NUMERIC=de_DE.UTF-8
LC_TIME=de_DE.UTF-8
LC_MONETARY=de_DE.UTF-8
LC_PAPER=de_DE.UTF-8
LC_MEASUREMENT=de_DE.UTF-8" > /etc/locale.conf
echo "KEYMAP=de-latin1" > /etc/vconsole.conf
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc

echo "$myhostname" > /etc/hostname
echo "127.0.0.1	localhost
::1		localhost
127.0.1.1	${myhostname}.localdomain	$myhostname" > /etc/hosts

curl "https://gitlab.com/dev.huthi/installtest/-/raw/master/package-list" > package-list
pacman -S --needed --noconfirm - < package-list
rm package-list
systemctl enable NetworkManager

useradd -m -g users -G wheel -s /bin/zsh $myusername
usermod -aG wheel,audio,video,optical,scanner,users $myusername
echo "root:$mypw" | chpasswd
echo "$myusername:$mypw" | chpasswd

sed /etc/sudoers -e 's/# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' -i

bootctl --path=/boot install

echo "title    Arch Linux
linux    /vmlinuz-linux
initrd   /initramfs-linux.img
options  root=${1}2 rw" > /boot/loader/entries/arch-uefi.conf

echo "title    Arch Linux Fallback
linux    /vmlinuz-linux
initrd   /initramfs-linux-fallback.img
options  root=${1}2 rw" > /boot/loader/entries/arch-uefi-fallback.conf

echo "default   arch-uefi
timeout   1" > /boot/loader/loader.conf
bootctl update

echo "Done"

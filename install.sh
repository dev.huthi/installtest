#!/bin/bash
set -euo pipefail

read -p "Device: " mdisk
echo

crap=$(ls /sys/firmware/efi/efivars)
ping archlinux.org -c 3

timedatectl set-ntp true
wipefs -a -f  $mdisk

parted $mdisk mklabel gpt
parted $mdisk mkpart "EFI" fat32 1MiB 500MiB
parted $mdisk set 1 esp on
parted $mdisk mkpart "Main" ext4 500MiB 100%

mkfs.fat -F32 ${mdisk}1
mkfs.ext4 ${mdisk}2

mount ${mdisk}2 /mnt
mkdir /mnt/boot
mount ${mdisk}1 /mnt/boot

reflector --country 'Germany' -l 5 -p https --sort rate --save /etc/pacman.d/mirrorlist
pacstrap /mnt base linux linux-firmware base-devel

genfstab -U -p /mnt >> /mnt/etc/fstab
curl "https://gitlab.com/dev.huthi/installtest/-/raw/master/second_step.sh" > /mnt/second_step.sh
arch-chroot /mnt chmod +x second_step.sh
arch-chroot /mnt ./second_step.sh $mdisk
